from django.conf.urls import patterns, include, url
from indexador_documentos.views import * 


urlpatterns = patterns('',

    url(r'^$','principal.views.index'),
    url(r'^logout/','principal.views.logout'),    
    url(r'^administrador/$','principal.views.perfilAdministrador'),
    url(r'^administrador/administradoressingle/','principal.views.obtenerAdministradoresSingle'),
    url(r'^administrador/administradores/','principal.views.obtenerAdministradores'),
    url(r'^usuario/','principal.views.perfilUsuario'),
)
