from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import RequestContext
from django.contrib import auth
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import ensure_csrf_cookie
import json
from principal.models import *
from principal.roles import Role


# Create your views here.

role = Role()

def index(request):
    
    if request.method == 'POST':
        usuario = request.POST['usuario']
        password = request.POST['password']
        user = auth.authenticate(username=usuario,password=password)
        
        if user is not None:
            if user.is_active:
                auth.login(request, user)
                
                if role.identificar(user,"Usuario"):
                    respuesta ={}
                    respuesta['success']= True
                    respuesta['rol']='usuario'
                    return HttpResponse(json.dumps(respuesta), content_type = "application/json")

                elif role.identificar(user,"Administrador"):
                    respuesta ={}
                    respuesta['success']= True
                    respuesta['rol']='administrador'
                    return HttpResponse(json.dumps(respuesta), content_type = "application/json")

            else:
                respuesta ={}
                respuesta['mensaje']='Cuenta no activa'
                respuesta['success']= False
                return HttpResponse(json.dumps(respuesta), content_type = "application/json")
        else:
            respuesta ={}
            respuesta['mensaje']='Usuario y/o password no existe'
            respuesta['success']= False
            return HttpResponse(json.dumps(respuesta), content_type = "application/json")
        
        
    return render_to_response('componentes/principal/index.html',context_instance=RequestContext(request))

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('http://127.0.0.1:8000/')

def perfilAdministrador(request):
    if role.identificar(request.user,"Administrador"):
        
        usuario = request.user
        
        return render_to_response('plataforma/admin/resumenAdministrador.html',locals(),context_instance=RequestContext(request))
    else:
        
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))

def perfilUsuario(request):
    if role.identificar(request.user,"Usuario"):
        usuario = request.user
        return render_to_response('plataforma/usuario/resumenUsuario.html',locals(),context_instance=RequestContext(request))
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))


def obtenerAdministradores(request):
    if role.identificar(request.user,"Administrador") or role.identificar(request.user,"Usuario") :
        usuarios = User.objects.all()
        administradores = []
        for usuario in usuarios:
            if role.identificar(usuario,"Administrador") and usuario != request.user :
                administradores.append(usuario)
                
        
        respuesta = render_to_string('componentes/principal/selectAdministradores.html',locals())
        return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))
    
def obtenerAdministradoresSingle(request):
    if role.identificar(request.user,"Administrador") or role.identificar(request.user,"Usuario") :
        usuarios = User.objects.all()
        administradores = []
        for usuario in usuarios:
            if role.identificar(usuario,"Administrador") and usuario != request.user :
                administradores.append(usuario)
                
        
        respuesta = render_to_string('componentes/principal/selectAdministradoresSingle.html',locals())
        return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))



#Ejemplo de respuesta en formato json respuesta['html'] = render_to_string('principal/index.html')
