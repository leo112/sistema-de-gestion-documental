from django.conf.urls import patterns, include, url
from django.conf import settings
from principal.views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Gestion_Documental.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^app/',include('principal.urls')),
    url(r'^indexador/',include('indexador_documentos.urls')),
    url(r'^administrador/',include('administrador_documentos.urls')),
    url(r'^$','principal.views.index'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),
)
