from django.conf.urls import patterns, include, url
from indexador_documentos.views import * 


urlpatterns = patterns('',

    url(r'^creardocumento/$','indexador_documentos.views.crearDocumento'),
    url(r'^test/$','indexador_documentos.views.testAjax'),
    url(r'^tiposdocumento/$','indexador_documentos.views.obtenerTiposDocumento'),
    url(r'^tiposversion/$','indexador_documentos.views.obtenerTiposVersion'),
    url(r'^documentosautor/$','indexador_documentos.views.obtenerDocumentosAutor'),
    url(r'^documentos/$','indexador_documentos.views.obtenerDocumentos'),
    url(r'^documentosrelacionado/$','indexador_documentos.views.obtenerDocumentosRelacionado'),
    url(r'^documentosfecha/$','indexador_documentos.views.obtenerDocumentosFecha'),
    url(r'^versiones/$','indexador_documentos.views.obtenerVersiones'),
    url(r'^crearversion/(?P<documento_id>\d+)$','indexador_documentos.views.crearVersion'),
    url(r'^eliminardocumento/','indexador_documentos.views.eliminarDocumento'),
)
