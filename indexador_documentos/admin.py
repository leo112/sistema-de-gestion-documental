from django.contrib import admin
from indexador_documentos.models import *
# Register your models here.

class DocumentoAdmin (admin.ModelAdmin):
    list_display = ('titulo','tipoDocumento','fecha')
    
admin.site.register(TipoDocumento)
admin.site.register(TipoVersion)
admin.site.register(Documento,DocumentoAdmin)
admin.site.register(Version)
