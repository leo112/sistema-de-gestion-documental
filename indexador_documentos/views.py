from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import RequestContext
from django.contrib import auth
from django.db.models import Q
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import ensure_csrf_cookie
import json
from indexador_documentos.models import *
from principal.roles import Role
# Create your views here.
from principal.roles import Role
role = Role()

def crearDocumento(request):
    if role.identificar(request.user,"Administrador"):
        if request.method == 'POST':
    
            documento = Documento()
            documento.titulo = request.POST['titulo']
            documento.descripcion = request.POST['descripcion']
            documento.tipoDocumento = TipoDocumento.objects.get(pk=request.POST['selecTipoDocumento'])
            documento.save()
            involucrados = request.POST.getlist('selectAdministradores')
            for involucrado in involucrados:
                nuevo = User.objects.get(pk=involucrado)
                documento.involucrados.add(nuevo)
            documento.save()
            version = Version()
            version.anotacion= request.POST['anotacion']
            version.archivo = request.FILES['archivo']
            version.tipoVersion = TipoVersion.objects.get(pk=1)
            version.autor= request.user
            version.save()
            documento.versiones.add(version)
            documento.save()
    
            return HttpResponseRedirect('http://127.0.0.1:8000/app/administrador')
    
        else:
            return render_to_response('componentes/indexador/crearDoc.html',context_instance=RequestContext(request))
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))


def crearVersion(request,documento_id):
    if role.identificar(request.user,"Administrador"):
        if request.method == 'POST':
            documento = Documento.objects.get(pk=request.POST['documento'])
            version = Version()
            version.anotacion = request.POST['anotacion'];
            version.tipoVersion = TipoVersion.objects.get(pk=request.POST['selecTipoVersion'])
            version.autor = request.user
            version.archivo = request.FILES['archivo']
            version.save()
            documento.versiones.add(version)
            documento.save()
            return HttpResponseRedirect('http://127.0.0.1:8000/app/administrador')
        
        id = documento_id
        
        return render_to_response('componentes/indexador/crearVersion.html',locals(),context_instance=RequestContext(request))
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))
def eliminarDocumento(request):
    if role.identificar(request.user,"Administrador"):
        if request.method == 'POST':
            id = request.POST['documento']; 
            documento = Documento.objects.get(pk=id)
            documento.versiones.all().delete()
            documento.delete()
            
            respuesta = {}
            respuesta['success'] = True 
            respuesta['mensaje'] = "Eliminacion Satisfactoria" 
            return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))
    
def testAjax(request):

    respuesta = {}
    respuesta['html'] = render_to_string('componentes/principal/index.html')
    return HttpResponse(json.dumps(respuesta), content_type = "application/json")


def obtenerTiposDocumento(request):
    if role.identificar(request.user,"Administrador"):
        tipos = TipoDocumento.objects.all()
               
        
        respuesta = render_to_string('componentes/indexador/selectTipoDocumento.html',locals())
        return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))



def obtenerTiposVersion(request):
    if role.identificar(request.user,"Administrador"):
        
        tipos = TipoVersion.objects.all().filter(~Q(nombreTipo='Inicial'))
               
        
        respuesta = render_to_string('componentes/indexador/selectTipoVersion.html',locals())
        return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))



def obtenerDocumentosAutor(request):
    if role.identificar(request.user,"Administrador"):    
        inicial = TipoVersion.objects.get(pk=1)
        versionesInicialesUsuario = Version.objects.all().filter(Q(autor=request.user),Q(tipoVersion=inicial))
        documentosUsuario = Documento.objects.all().filter(versiones = versionesInicialesUsuario)
               
        
        respuesta = render_to_string('componentes/indexador/documentosUsuario.html',locals())
        return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
        return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))



def obtenerDocumentosRelacionado(request):
    if role.identificar(request.user,"Administrador"):   
 
        documentosUsuario = Documento.objects.all().filter(involucrados = request.user)       
        
        respuesta = render_to_string('componentes/indexador/documentosUsuario.html',locals())
        return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
            return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))

def obtenerDocumentos(request):
    if role.identificar(request.user,"Administrador"):       
        versionesUsuario = Version.objects.all().filter(autor=request.user)
        documentosUsuario = Documento.objects.all().distinct().filter(Q(versiones=versionesUsuario) | Q(involucrados=request.user))
               
        
        respuesta = render_to_string('componentes/indexador/documentosUsuario.html',locals())
        return HttpResponse(json.dumps(respuesta), content_type = "application/json")    
    else:
            return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))
        
def obtenerDocumentosFecha(request):
    if role.identificar(request.user,"Administrador"):       
        if request.method == 'POST':
            fecha = request.POST['fecha']
            versionesUsuario = Version.objects.all().filter(Q(autor=request.user),Q(fecha=fecha))
            documentosUsuario = Documento.objects.all().distinct().filter(versiones = versionesUsuario)
            
            respuesta = render_to_string('componentes/indexador/documentosUsuario.html',locals())
            return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
            return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))
        
        
def obtenerVersiones(request):
    if role.identificar(request.user,"Administrador"):       
        if request.method == 'POST':
            documentoId = request.POST['documento']
            documento = Documento.objects.get(pk=documentoId)
            versiones = documento.versiones.all()
            respuesta = render_to_string('componentes/indexador/versiones.html',locals())
            return HttpResponse(json.dumps(respuesta), content_type = "application/json")
    else:
            return render_to_response('componentes/principal/error.html',locals(),context_instance=RequestContext(request))
    
    
    