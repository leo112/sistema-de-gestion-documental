from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class TipoVersion (models.Model):
    nombreTipo = models.CharField(max_length=100)
    def __unicode__(self):
        return self.nombreTipo
    
class TipoDocumento (models.Model):
    nombreTipo = models.CharField(max_length=100)
    def __unicode__(self):
        return self.nombreTipo

   
class Version (models.Model):
    tipoVersion = models.ForeignKey(TipoVersion, blank=True)
    anotacion = models.TextField(max_length=500)
    archivo = models.FileField(upload_to='versiones')
    autor = models.ForeignKey(User)
    fecha = models.DateField(auto_now_add=True)
    def __unicode__(self):
        return self.tipoVersion.nombreTipo
        
class Documento (models.Model):
    titulo = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=500)
    tipoDocumento = models.ForeignKey(TipoDocumento,blank=True)
    versiones = models.ManyToManyField(Version, blank=True)
    involucrados = models.ManyToManyField(User, blank=True)
    fecha = models.DateField(auto_now_add=True)

 
