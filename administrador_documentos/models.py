from django.db import models

# Create your models here.
class TipoPlantilla(models.Model):
    nombreTipo = models.CharField(max_length=100)
    def __unicode__(self):
        return self.nombreTipo
    
class Plantilla (models.Model):
    titulo = models.CharField(max_length=100)
    plantilla = models.FileField(upload_to='plantillas')
    tipo = models.ForeignKey(TipoPlantilla)
    descripcion = models.TextField(max_length=500)
    def __unicode__(self):
        return self.titulo