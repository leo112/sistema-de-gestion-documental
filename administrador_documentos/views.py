from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import RequestContext
from django.contrib import auth
from django.db.models import Q
from django.template.loader import render_to_string
import json
from administrador_documentos.models import *
from principal.roles import Role


def verPlantillas(request):
    
    plantillas = Plantilla.objects.all() 
    
    respuesta = render_to_string('componentes/administrador/plantillas.html',locals())
    return HttpResponse(json.dumps(respuesta), content_type = "application/json")

